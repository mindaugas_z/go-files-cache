# Golang Files Cache 


start daemon
```
./cache -db ./db.db -bind 127.0.0.1:1250 -dir ./cache 
```


upload file
```
curl -X POST --data @file.bin "localhost:1250/put?file=test&ttl=60"
```

get file
```
curl localhost:1250/get?file=test
```
