module files_cache_ttl

go 1.12

require (
	github.com/etcd-io/bbolt v1.3.2
	gopkg.in/ini.v1 v1.42.0
)
