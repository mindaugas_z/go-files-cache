package main

import (
	"github.com/etcd-io/bbolt"

	"bytes"
	"encoding/binary"
	"errors"
	"flag"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

var (
	kvdb *bbolt.DB
)

var (
	flagBind     = flag.String("bind", ":1250", "-bind 127.0.0.1:1250")
	flagCacheDir = flag.String("dir", "./cache", "-dir /mnt/cache")
	flagDB       = flag.String("db", "./db.db", "-db ./db.db")
)

var (
	bucketTimes   = []byte("times")
	bucketFiles   = []byte("files")
	bucketHeaders = []byte("headers")
)

func main() {
	println("Service starting..")
	flag.Parse()

	var err error

	println("Initializing database...")
	kvdb, err = bbolt.Open(*flagDB, 0755, nil)
	if err != nil {
		panic(err)
	}

	println("Creating buckets...")
	kvdb.Update(func(tx *bbolt.Tx) error {
		tx.CreateBucketIfNotExists(bucketFiles)
		tx.CreateBucketIfNotExists(bucketHeaders)
		tx.CreateBucketIfNotExists(bucketTimes)
		return nil
	})

	println("Creating cache dir")
	os.MkdirAll(*flagCacheDir, 0755)

	go func() {
		for {
			time.Sleep(1 * time.Second)
			kvdb.Update(func(tx *bbolt.Tx) error {
				timesBucket := tx.Bucket(bucketTimes)
				timesCursor := timesBucket.Cursor()
				filesBucket := tx.Bucket(bucketFiles)
				headersBucket := tx.Bucket(bucketHeaders)

				now := time.Now().UnixNano()
				n := 0
				for k, v := timesCursor.First(); k != nil && bytes.Compare(k, itob(int(now))) < 0; k, v = timesCursor.Next() {
					if n > 50 {
						break
					}
					fullFilePath := *flagCacheDir + "/" + string(strconv.Itoa(int(btoi(k))))
					// log.Printf("Deleting %s", fullFilePath)
					os.Remove(fullFilePath)
					filesBucket.Delete(v)
					timesBucket.Delete(k)
					headersBucket.Delete(v)
					n++
				}
				return nil
			})
		}
	}()

	http.HandleFunc("/get", func(w http.ResponseWriter, r *http.Request) {
		file := r.URL.Query().Get("file")
		localFile := []byte(nil)

		err := kvdb.View(func(tx *bbolt.Tx) error {
			localFile = tx.Bucket(bucketFiles).Get([]byte(file))
			if localFile == nil {
				return errors.New("Not Found")
			}
			return nil
		})

		if err != nil {
			http.NotFound(w, r)
			return
		}

		fh, err := os.Open(*flagCacheDir + "/" + string(strconv.Itoa(int(btoi(localFile)))))
		if err != nil {
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		}
		defer fh.Close()
		header := ""
		kvdb.View(func(tx *bbolt.Tx) error {
			headersBucket := tx.Bucket(bucketHeaders)
			header = string(headersBucket.Get([]byte(file)))
			return nil
		})
		w.Header().Set("Content-Type", header)
		io.Copy(w, fh)

	})

	http.HandleFunc("/put", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "POST" {
			http.Error(w, "Bad Request", http.StatusBadRequest)
			return
		}
		file := r.URL.Query().Get("file")
		ttl, err := strconv.ParseInt(r.URL.Query().Get("ttl"), 10, 64)
		contentType := r.Header.Get("Content-Type")

		if err != nil || file == "" {
			http.Error(w, "Bad request", http.StatusBadRequest)
			return
		}

		expires := time.Now().UnixNano() + ttl*time.Second.Nanoseconds()

		kvdb.Update(func(tx *bbolt.Tx) error {
			timesBucket := tx.Bucket(bucketTimes)
			filesBucket := tx.Bucket(bucketFiles)
			headersBucket := tx.Bucket(bucketHeaders)

			timesBucket.Put(itob(int(expires)), []byte(file))
			filesBucket.Put([]byte(file), itob(int(expires)))
			headersBucket.Put([]byte("file"), []byte(contentType))

			return nil
		})
		fh, err := os.OpenFile(*flagCacheDir+"/"+strconv.Itoa(int(expires)), os.O_CREATE|os.O_RDWR, 0755)
		if err != nil {
			log.Print(err)
			http.Error(w, "Bad Request", http.StatusBadRequest)
			return
		}
		defer r.Body.Close()
		io.Copy(fh, r.Body)

	})

	println("Binding on port...")
	log.Fatal(http.ListenAndServe(*flagBind, nil))
}

func itob(v int) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(v))
	return b
}

func btoi(v []byte) uint64 {
	return binary.BigEndian.Uint64(v)
}
